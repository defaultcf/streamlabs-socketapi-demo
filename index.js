const io = require('socket.io')();
const response = require('./response.js');

const getResponse = () => {
  const keys = Object.keys(response);
  const i = Math.round(Math.random() * 10) % keys.length;
  const key = keys[i];
  return response[key];
};

io.on('connection', socket => {
  const timer = setInterval(() => {
    console.log('Hello from Timer;)');
    const response = getResponse();
    console.log(response);
    socket.emit('event', response);
  }, 3000);

  socket.on('disconnect', () => {
    clearInterval(timer);
  });
});

io.listen(3000);
