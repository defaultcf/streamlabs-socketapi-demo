module.exports = {
  'superchat': {
    "type": "superchat",
    "message": [
      {
        "id": "LCC.Cg8KDQoLWmpUemdzdU1vM2sSHAoaQ05PcW01UzJ3dElDRll2WkhBb2RLd0lBOWc0.028141892513880107",
        "channelId": "UCec4hVEu3ZXE8qlRtUeK0DA",
        "channelUrl": "http://www.youtube.com/channel/UCec4hVEu3ZXE8qlRtUeK0DA",
        "name": "Kappa Lord",
        "comment": "love the stream",
        "amount": "2000000",
        "currency": "USD",
        "displayString": "$2.00",
        "messageType": 2,
        "createdAt": "2017-08-22 00:51:57",
        "_id": "ad46c7e468331c5e39373e05876fa17e"
      }
    ],
    "for": "youtube_account"
  },

  'subscription': {
    "type": "follow",
    "message": [
      {
        "publishedAt": "2017-08-22 00:25:57",
        "id": "UCec4hVEu3ZXE8qlRtUeK0DA",
        "name": "Kappa Lord",
        "_id": "6af0964e548a6d93c192c9e31e0959cc"
      }
    ],
    "for": "youtube_account"
  },


  'membership': {
    "type": "subscription",
    "message": [
      {
        "sponsorSince": "2017-08-22 00:45:48",
        "id": "UCec4hVEu3ZXE8qlRtUeK0DA",
        "name": "Kappa Lord",
        "channelUrl": "http://www.youtube.com/channel/UCec4hVEu3ZXE8qlRtUeK0DA",
        "months": 3,
        "_id": "33d396edab6e2e2e8b37a7b6f1b60b82"
      }
    ],
    "for": "youtube_account"
  },

};
